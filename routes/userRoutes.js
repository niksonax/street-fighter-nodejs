const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
// /api/users

router.get(
  "/",
  function (req, res, next) {
    const users = UserService.getAll();
    res.data = users;
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    const userID = req.params.id;
    const user = UserService.search({ id: userID });
    if (user) {
      res.data = user;
    } else {
      res.err = { message: "User not found" };
    }
    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  function (req, res, next) {
    const userData = req.body;
    if (
      UserService.search({ email: userData.email }) ||
      UserService.search({ phoneNumber: userData.phoneNumber })
    ) {
      res.err = { message: "User with this credentials already exists" };
    } else {
      const user = UserService.create(userData);
      res.data = user;
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  function (req, res, next) {
    const userID = req.params.id;
    const dataToUpdate = req.body;
    if (UserService.search({ id: userID })) {
      const updatedUser = UserService.update(userID, dataToUpdate);
      res.data = updatedUser;
    } else {
      res.err = { message: "User not found" };
    }
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    const userID = req.params.id;
    if (UserService.search({ id: userID })) {
      const deletedUser = UserService.delete(userID);
      res.data = deletedUser;
    } else {
      res.err = { message: "User not found" };
    }
    next();
  },
  responseMiddleware
);

module.exports = router;

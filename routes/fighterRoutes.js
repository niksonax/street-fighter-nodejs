const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter

router.get(
  "/",
  function (req, res, next) {
    const fighters = FighterService.getAll();
    res.data = fighters;
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    const fighterID = req.params.id;
    const fighter = FighterService.search({ id: fighterID });
    if (fighter) {
      res.data = fighter;
    } else {
      res.err = { message: "Fighter not found" };
    }
    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  function (req, res, next) {
    const fighterData = req.body;
    if (FighterService.search({ name: fighterData.name })) {
      res.err = { message: "Fighter with this name already exists" };
    } else {
      const fighter = FighterService.create(fighterData);
      res.data = fighter;
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  function (req, res, next) {
    const fighterID = req.params.id;
    const dataToUpdate = req.body;
    if (FighterService.search({ id: fighterID })) {
      const updatedFighter = FighterService.update(fighterID, dataToUpdate);
      res.data = updatedFighter;
    } else {
      res.err = { message: "Fighter not found" };
    }
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    const fighterID = req.params.id;
    if (FighterService.search({ id: fighterID })) {
      const deletedFighter = FighterService.delete(fighterID);
      res.data = deletedFighter;
    } else {
      res.err = { message: "Fighter not found" };
    }
    next();
  },
  responseMiddleware
);

module.exports = router;

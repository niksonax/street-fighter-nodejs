const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAll() {
    const items = FighterRepository.getAll();
    if (!items) return null;
    return items;
  }

  create(data) {
    const item = FighterRepository.create(data);
    if (!item) return null;
    return item;
  }

  update(id, data) {
    const updatedItem = FighterRepository.update(id, data);
    if (!updatedItem) return null;
    return updatedItem;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) return null;
    return item;
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item) return null;
    return item;
  }
}

module.exports = new FighterService();

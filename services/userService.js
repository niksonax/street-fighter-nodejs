const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  create(data) {
    const item = UserRepository.create(data);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, data) {
    const updatedItem = UserRepository.update(id, data);
    if (!updatedItem) {
      return null;
    }
    return updatedItem;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();

const { fighter } = require("../models/fighter");
const fighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const fighterData = req.body;
  let { name, health, power, defense } = fighterData;

  if (!dataFieldsValid(fighterData))
    return res.status(400).json({
      error: true,
      message: "Request body has invalid field(s) or empty",
    });

  if (isFighterNameExists(name)) {
    return res.status(400).json({
      error: true,
      message: "Fighter with this name alredy exists",
    });
  }

  if (!health) {
    req.body.health = 100;
  }

  if (!name) {
    return res
      .status(400)
      .json({ error: true, message: "Fighter's name wasn't defined" });
  } else if (health < 80 || health > 120) {
    return res.status(400).json({
      error: true,
      message: "Fighter's health should be in range 80-120",
    });
  } else if (!power) {
    return res
      .status(400)
      .json({ error: true, message: "Fighter's power wasnt defined" });
  } else if (power < 1 || power > 100) {
    return res.status(400).json({
      error: true,
      message: "Fighter's power should be in range 1-100",
    });
  } else if (!defense) {
    return res
      .status(400)
      .json({ error: true, message: "Fighter's defense wasnt defined" });
  } else if (defense < 1 || defense > 10) {
    return res.status(400).json({
      error: true,
      message: "Fighter's defense should be in range 1-10",
    });
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update

  const dataToUpdate = req.body;

  if (!dataFieldsValid(dataToUpdate))
    return res.status(400).json({
      error: true,
      message: "Request body has invalid field(s) or empty",
    });

  for (let field in dataToUpdate) {
    const value = dataToUpdate[field];
    if (field === "name" && isFighterNameExists(value)) {
      return res.status(400).json({
        error: true,
        message: "Fighter with this name alredy exists",
      });
    }

    if (field === "health") {
      if (!(typeof value === "number") || value < 80 || value > 120) {
        return res.status(400).json({
          error: true,
          message: "Fighter's health should be in range 80-120",
        });
      }
    } else if (field === "power") {
      if (!(typeof value === "number") || value < 1 || value > 100) {
        return res.status(400).json({
          error: true,
          message: "Fighter's power should be in range 1-100",
        });
      }
    } else if (field === "defense") {
      if (!(typeof value === "number") || value < 1 || value > 10) {
        return res.status(400).json({
          error: true,
          message: "Fighter's defense should be in range 1-10",
        });
      }
    }
  }

  next();
};

function dataFieldsValid(data) {
  if (Object.keys(data).length === 0) return false;
  for (let field in data) {
    const areFieldsSameType = typeof fighter[field] === typeof data[field];
    if (!Object.keys(fighter).includes(field) || !areFieldsSameType) {
      return false;
    }
  }
  return true;
}

function isFighterNameExists(name) {
  const fighters = fighterService.getAll();
  for (let fighter of fighters) {
    if (fighter.name.toLowerCase() === name.toLowerCase()) {
      return true;
    }
  }
  return false;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

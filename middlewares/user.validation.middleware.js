const { user } = require("../models/user");

const mailRegex = /^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/;
const phoneRegex = /\+*380[0-9]{9}$/;

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const userData = req.body;
  let { firstName, lastName, email, phoneNumber, password } = userData;

  if (!dataFieldsValid(userData))
    return res.status(400).json({
      error: true,
      message: "Request body has invalid field(s) or empty",
    });

  if (!firstName) {
    return res
      .status(400)
      .json({ error: true, message: "First name wasn`t defined" });
  } else if (!lastName) {
    return res
      .status(400)
      .json({ error: true, message: "Last name wasn`t defined" });
  } else if (!email || !email.match(mailRegex)) {
    return res.status(400).json({ error: true, message: "Invalid email" });
  } else if (!phoneNumber || !phoneNumber.match(phoneRegex)) {
    return res
      .status(400)
      .json({ error: true, message: "Invalid phone number" });
  } else if (!password || password.length < 3) {
    return res.status(400).json({
      error: true,
      message: "Password should be at least 3 characters long",
    });
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const dataToUpdate = req.body;

  if (!dataFieldsValid(dataToUpdate))
    return res.status(400).json({
      error: true,
      message: "Request body has invalid field(s) or empty",
    });

  for (let field in dataToUpdate) {
    const value = dataToUpdate[field];
    console.log(value);
    if (field === "firstName" && value === "") {
      return res.status(400).json({
        error: true,
        message: "FirstName shouldn't be empty",
      });
    } else if (field === "lastName" && value === "") {
      return res.status(400).json({
        error: true,
        message: "LastName shouldn't be empty",
      });
    } else if (field === "email" && !value.match(mailRegex)) {
      return res.status(400).json({
        error: true,
        message: "Invalid email",
      });
    } else if (field === "phoneNumber" && !value.match(phoneRegex)) {
      return res.status(400).json({
        error: true,
        message: "Invalid phone number",
      });
    } else if (field === "password" && value.length < 3) {
      return res.status(400).json({
        error: true,
        message: "Invalid password",
      });
    }
  }

  next();
};

function dataFieldsValid(data) {
  if (Object.keys(data).length === 0) return false;
  for (let field in data) {
    const areFieldsSameType = typeof user[field] === typeof data[field];
    if (!Object.keys(user).includes(field) || !areFieldsSameType) {
      return false;
    }
  }
  return true;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
